﻿#include <iostream>

using namespace std;

class Animal
{
public:
	Animal()
	{

	}

	 virtual void Voice()
	{
		
	}

};

class Dog : public Animal
{
public:
	Dog()
	{

	}

	void Voice() override
	{
		cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	Cat()
	{

	}

	void Voice() override
	{
		cout << "Meow!\n";
	}
};

class Goose : public Animal
{
public:

	Goose()
	{

	}

	void Voice() override
	{
		cout << "Ga Ga Ga!\n";
	}
};


int main()
{
	Cat* c = new Cat;
	Dog* d = new Dog;
	Goose* g = new Goose;
	
	const int size = 6;

	Animal *arr[size] = {c,d,g,d,g,c};
	arr[0] = c;
	arr[1] = d;
	arr[2] = g;

	for (int i = 0; i < size; i++)
	{
	  arr[i]->Voice();
	}


}

